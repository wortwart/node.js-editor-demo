A directory viewer and text file editor for Node.js.

This tutorial project was started by Herbert Braun and featured in c't Magazin 4/2014. More information: www.woerter.de/textverzeichnis/#filter/id=168 (in German).
